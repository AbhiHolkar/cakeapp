# CakeApp
Cake App application displays list of cakes with their name , description and image. App uses a test end point to fetch the cake list.
On failure to fetch the list app shows generic Api error and refresh button to Retry. Same Refresh button can used to re-fetch and refresh the cake list.


# App is coded in kotlin

- Application uses MVVM architecture & CLEAN architecture & principles of SOLID programming.
- App uses Android architecture components based on MVVM.
- Retrofit is used for Network layer.
- RxJava2 is used for Background/Asychronous operations like Network call/ response fetch.
- Kotlin "Data" class is used for models.
- Picasso is used to fetch/resize images.
- Application uses Mockito for Junit test cases.
- Repository and Use case concept of Clean architecture makes code modular and testable.
- Recylcer view is used to display list of cakes.

# Pre-Requiste

- Android studio . Kotlin.

# Run/Test

- App has been tested on device
- App unit test covers Viewmodel for demonstration only.

# Techincal Debt/TODOs

- To use Dagger for all dependencies.
- Change RxJava calls to Kotlin coroutines
- Add refresh on action bar and remove Refresh button.
- Add more test coverage.
- Add persistence to save response using any DB(Realm/ORM/SQLite)
- UI code on fragments

# Known bug

TBD persistence
