package com.waracle.cakelistapp.view

interface OnItemClickListener<T> {
    fun onItemClicked(item : T)
}