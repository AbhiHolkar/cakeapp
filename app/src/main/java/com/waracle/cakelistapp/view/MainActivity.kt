package com.waracle.cakelistapp.view


import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.waracle.cakelistapp.R
import com.waracle.cakelistapp.adapter.CakeListAdapter
import com.waracle.cakelistapp.model.Cake
import com.waracle.cakelistapp.model.WaracleModel
import com.waracle.cakelistapp.usecase.GetCakeListUseCase
import com.waracle.cakelistapp.viewmodel.CakeListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnItemClickListener<Cake> {


    private lateinit var cakeListViewModel: CakeListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        list.layoutManager = LinearLayoutManager(this)
        val getCakeListUseCase = GetCakeListUseCase()

        //cakeListViewModel = ViewModelProviders.of(this).get(CakeListViewModel::class.java)
        cakeListViewModel= ViewModelProviders.of(this,object : ViewModelProvider.Factory{
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return CakeListViewModel(getCakeListUseCase) as T
            }

        })[CakeListViewModel::class.java]
        cakeListViewModel.getCakeList().observe(this, Observer<WaracleModel> { waracleModel ->
            if (waracleModel?.cakeList != null) {
                showCakeList(waracleModel.cakeList)
            } else {
                showError()
            }
        })
    }


    fun showCakeList(cakeList: List<Cake>) {
        val cakeListAdapter = CakeListAdapter(cakeList, this, this)
        list.adapter = cakeListAdapter
        showRefresh()
        errorview.visibility = View.GONE
        progress.visibility = View.GONE
        list.visibility = View.VISIBLE
    }

    fun showError() {
        list.visibility = View.GONE
        progress.visibility = View.GONE
        errorview.visibility = View.VISIBLE
        showRefresh()

    }

    //TODO - use action bar refresh for this
    private fun showRefresh() {
        refresh_button.visibility = View.VISIBLE
        refresh_button.setOnClickListener {
            progress.visibility = View.VISIBLE
            cakeListViewModel.getCakeList()
        }
    }

    override fun onItemClicked(item: Cake) {
        showDialog(cakeItem = item)
    }

    private fun showDialog(cakeItem: Cake) {
        val dialogBuilder = AlertDialog.Builder(this)
        with(dialogBuilder) {
            setMessage(cakeItem.desc)
            setTitle(cakeItem.title)
            setPositiveButton(getString(R.string.ok_label)) { dialog, okButton -> dialog.dismiss() }
            show()
        }

    }
}
