package com.waracle.cakelistapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.waracle.cakelistapp.R
import com.waracle.cakelistapp.model.Cake
import com.waracle.cakelistapp.view.OnItemClickListener
import com.waracle.cakelistapp.viewholder.CakeItemVH
import kotlinx.android.synthetic.main.item_layout.view.*


class CakeListAdapter(private val cakeList: List<Cake>, private val context: Context, private val itemClickListener: OnItemClickListener<Cake>) : RecyclerView.Adapter<CakeItemVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CakeItemVH {
        val view = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false)
        return CakeItemVH(view)
    }

    override fun getItemCount(): Int {
        return cakeList.size
    }

    override fun onBindViewHolder(holder: CakeItemVH, position: Int) {
        val cakeItem = cakeList[position]
        holder.itemView.cakeTitle.text = cakeItem.title
        holder.itemView.setOnClickListener { v -> itemClickListener.onItemClicked(cakeItem) }
        Picasso.with(context).load(cakeItem.image).resize(480, 320).into(holder.itemView.cakeImage)
    }
}