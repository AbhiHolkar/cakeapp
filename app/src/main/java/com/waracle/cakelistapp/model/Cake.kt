package com.waracle.cakelistapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Cake(
        val desc: String,
        val image: String,
        val title: String
) : Parcelable