package com.waracle.cakelistapp.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.waracle.cakelistapp.model.Cake
import com.waracle.cakelistapp.model.WaracleModel
import com.waracle.cakelistapp.usecase.GetCakeListUseCase
import io.reactivex.observers.DisposableObserver

class CakeListViewModel(val getCakeListUseCase: GetCakeListUseCase) : ViewModel() {


    lateinit var cakeListViewModel: MutableLiveData<WaracleModel>

    fun getCakeList(): MutableLiveData<WaracleModel> {

        //TODO - use DI for all dependencies
        if (!::cakeListViewModel.isInitialized) {
            cakeListViewModel = MutableLiveData()
        }
        getCakeListUseCase.getCakeList().subscribe(object : DisposableObserver<List<Cake>>() {
            override fun onComplete() {

            }

            override fun onNext(cakeList: List<Cake>) {
                cakeList.forEachIndexed { index, cakeResponse ->
                    //Log.d("Cake", "Name" + cakeResponse.title)
                }
                var sortedCakeList = sortCakeList(cakeList)
                cakeListViewModel.value = WaracleModel(sortedCakeList, null)
            }

            override fun onError(e: Throwable) {

                //TODO - use error types(enums) for different errors- no network , server API exception , etc
                cakeListViewModel.value = WaracleModel(null, RuntimeException())
            }

        })

        return cakeListViewModel
    }

    private fun sortCakeList(cakeList: List<Cake>): List<Cake> {
        var sortedCakeList = cakeList.distinctBy { it.title }
        sortedCakeList = sortedCakeList.sortedWith(compareBy { it.title })
        return sortedCakeList
    }

}