package com.waracle.cakelistapp.repository

import com.waracle.cakelistapp.model.Cake
import com.waracle.cakelistapp.network.APIClient
import com.waracle.cakelistapp.network.CakeListService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CakeRepository {

    fun getCakeList(): Observable<List<Cake>> {

        val apiClient = APIClient()
        return apiClient.getAPIClient()
                .create(CakeListService::class.java)
                .getCakeList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }
}