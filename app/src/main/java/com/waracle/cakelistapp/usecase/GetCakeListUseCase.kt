package com.waracle.cakelistapp.usecase

import com.waracle.cakelistapp.model.Cake
import com.waracle.cakelistapp.repository.CakeRepository
import io.reactivex.Observable

class GetCakeListUseCase {

    //TODO - Dependencies can be injected using Dagger
    fun getCakeList(): Observable<List<Cake>> {
        val cakeRepository = CakeRepository()
        return cakeRepository.getCakeList()

    }

}