package com.waracle.cakelistapp.viewmodel

import com.nhaarman.mockitokotlin2.mock
import com.waracle.cakelistapp.model.Cake
import com.waracle.cakelistapp.usecase.GetCakeListUseCase
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class CakeListViewModelTest {


    private lateinit var mockGetListUseCase: GetCakeListUseCase
    private lateinit var getCakeListViewModel: CakeListViewModel

    @Before
    fun setUp() {

        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

        mockGetListUseCase = mock(GetCakeListUseCase::class.java)
        getCakeListViewModel = CakeListViewModel(mockGetListUseCase)

    }

    @After
    fun tearDown() {

        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }

    @Test
    fun getCakeList() {
        //Given:
        val testObserver = TestObserver<List<Cake>>()
        val mockObservable = mock<Observable<List<Cake>>> {

        }
        mockObservable.subscribe(testObserver)

        //When :
        Mockito.`when`(mockGetListUseCase.getCakeList()).thenReturn(mockObservable)
        getCakeListViewModel.getCakeList()

        //Then :
        assertNotNull(mockGetListUseCase)
        testObserver.assertNoErrors()
        testObserver.onComplete()
        testObserver.assertComplete()



    }


    @Test(expected = Throwable::class)
    fun getCakeListWithException(){

        //Given:
        val testObserver = TestObserver<List<Cake>>()
        val mockException =  mock(Throwable::class.java)
        val mockObservable = mock<Observable<List<Cake>>> {

        }
        mockObservable.subscribe(testObserver)

        //When :
        Mockito.`when`(mockGetListUseCase.getCakeList()).thenThrow(mockException)
        getCakeListViewModel.getCakeList()


        //Then :
        assertNotNull(mockGetListUseCase)
        testObserver.assertError(mockException)
        testObserver.onComplete()
        testObserver.assertComplete()

    }
}